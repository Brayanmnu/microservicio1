package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.VendedorProducto;
import com.everis.apirest.model.repository.VendedorProductoRepository;

@Service
public class VendedorProductoServiceImpl implements VendedorProductoService{

	@Autowired
	VendedorProductoRepository vendedorProductoRepository;
	
	@Override
	public VendedorProducto crearVendedorProducto(VendedorProducto request) throws Exception {
		return vendedorProductoRepository.save(request);
	}

	@Override
	public VendedorProducto obtenerVendedorProductoPorId(Long id) throws Exception {
		return vendedorProductoRepository.findById(id).orElseThrow(()->new Exception("Vendedor no encontrado"));
	}
	
}
