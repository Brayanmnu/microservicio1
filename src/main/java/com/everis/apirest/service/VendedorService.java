package com.everis.apirest.service;

import com.everis.apirest.controller.resource.VendedorResource;
import com.everis.apirest.model.entity.Vendedor;

public interface VendedorService {
	public Vendedor obtenerVendedorPorId(Long id) throws Exception;
	public Vendedor crearVendedor(Vendedor request);
	public Iterable<Vendedor> obtenerVendedores();
}
