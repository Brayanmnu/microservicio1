package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.controller.resource.VendedorResource;
import com.everis.apirest.model.entity.Vendedor;
import com.everis.apirest.model.repository.VendedorRepository;

@Service
public class VendedorServiceImpl implements VendedorService{

	@Autowired
	VendedorRepository vendedorRepository;
	
	@Override
	public Vendedor obtenerVendedorPorId(Long id) throws Exception{
		return vendedorRepository.findById(id).orElseThrow(()->new Exception("Vendedor no encontrado"));
	}

	@Override
	public Vendedor crearVendedor(Vendedor request) {
		return vendedorRepository.save(request);
	}

	@Override
	public Iterable<Vendedor> obtenerVendedores() {
		return vendedorRepository.findAll();
	}

}
