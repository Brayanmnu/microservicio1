package com.everis.apirest.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.apirest.controller.resource.ProductoResource;

@FeignClient("PRODUCTO")
public interface ProductoService {
	@GetMapping("producto/{id}")
	public ProductoResource obtenerProductoPorId(@PathVariable("id") Long id)  throws Exception ;
}
