package com.everis.apirest.service;

import com.everis.apirest.model.entity.VendedorProducto;

public interface VendedorProductoService {
	public VendedorProducto obtenerVendedorProductoPorId(Long id) throws Exception;
	public VendedorProducto crearVendedorProducto(VendedorProducto request) throws Exception;
}
