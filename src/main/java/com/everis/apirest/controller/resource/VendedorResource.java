package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class VendedorResource {
	private Long idVendedor;
	private String nombres;
	private String apellidos;
	private String cargo;
}
