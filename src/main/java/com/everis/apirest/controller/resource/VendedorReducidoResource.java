package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class VendedorReducidoResource {
	private String nombres;
	private String apellidos;
	private String cargo;
}
