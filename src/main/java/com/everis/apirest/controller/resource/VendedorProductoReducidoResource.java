package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class VendedorProductoReducidoResource {

	private BigDecimal precio;
	private Long idProducto;
	private Long idVendedor;
}
