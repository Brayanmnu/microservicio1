package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.controller.resource.VendedorProductoReducidoResource;
import com.everis.apirest.controller.resource.VendedorProductoResource;
import com.everis.apirest.controller.resource.VendedorReducidoResource;
import com.everis.apirest.controller.resource.VendedorResource;
import com.everis.apirest.model.entity.Vendedor;
import com.everis.apirest.model.entity.VendedorProducto;
import com.everis.apirest.service.ProductoService;
import com.everis.apirest.service.VendedorProductoService;
import com.everis.apirest.service.VendedorService;

@RestController
public class ApiController {
	@Autowired
	VendedorService vendedorService;
	
	@Autowired
	VendedorProductoService vendedorProductoService;
	
	@Autowired
	ProductoService productoService;
	
	
	@PostMapping("/vendedor")
	public VendedorResource crearVendedor(@RequestBody VendedorReducidoResource request) throws Exception {
		Vendedor vendedor = new Vendedor();
		vendedor.setApellidos(request.getApellidos());
		vendedor.setCargo(request.getCargo());
		vendedor.setNombres(request.getNombres());
		Vendedor vendedorOut = vendedorService.crearVendedor(vendedor);
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setApellidos(vendedorOut.getApellidos());
		vendedorResource.setCargo(vendedorOut.getCargo());
		vendedorResource.setIdVendedor(vendedorOut.getIdVendedor());
		vendedorResource.setNombres(vendedorOut.getNombres());
		return vendedorResource;
	}
	
	@GetMapping("vendedor/{id}")
	public VendedorResource obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception {
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(id);
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setApellidos(vendedor.getApellidos());
		vendedorResource.setCargo(vendedor.getCargo());
		vendedorResource.setIdVendedor(vendedor.getIdVendedor());
		vendedorResource.setNombres(vendedor.getNombres());
		return vendedorResource;
	}
	
	
	@PostMapping("/vendedorProducto")
	public ResponseEntity<VendedorProductoResource> crearVendedorProducto(@RequestBody VendedorProductoReducidoResource request) throws Exception {
		VendedorProducto vendedorProducto = new VendedorProducto();
		
		vendedorProducto.setProductoIdProducto(request.getIdProducto());
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(request.getIdVendedor());
		vendedorProducto.setVendedor(vendedor);
		vendedorProducto.setPrecio(request.getPrecio());
		
		
		VendedorProducto vendedorProductoOut = vendedorProductoService.crearVendedorProducto(vendedorProducto);
		VendedorProductoResource vendedorProductoResource = new VendedorProductoResource();
		
		vendedorProductoResource.setIdVendedorProducto(vendedorProductoOut.getIdVendedorProducto());
		vendedorProductoResource.setPrecio(vendedorProductoOut.getPrecio());
		
		vendedorProductoResource.setProducto( productoService
				.obtenerProductoPorId(vendedorProductoOut.getProductoIdProducto()));
		
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setIdVendedor(vendedorProductoOut.getVendedor().getIdVendedor());
		vendedorResource.setApellidos(vendedorProductoOut.getVendedor().getApellidos());
		vendedorResource.setCargo(vendedorProductoOut.getVendedor().getCargo());
		vendedorResource.setNombres(vendedorProductoOut.getVendedor().getNombres());
		
		vendedorProductoResource.setVendedor(vendedorResource);
		
		return  new ResponseEntity<>(vendedorProductoResource, HttpStatus.CREATED);
	}
	
	@GetMapping("vendedorProducto/{id}")
	public VendedorProductoResource obtenerVendedorProductoPorId(@PathVariable("id") Long id) throws Exception{
		VendedorProducto vendedorProducto = vendedorProductoService.obtenerVendedorProductoPorId(id);
		VendedorProductoResource vendedorProductoResource = new VendedorProductoResource();
		
		vendedorProductoResource.setIdVendedorProducto(vendedorProducto.getIdVendedorProducto());
		vendedorProductoResource.setPrecio(vendedorProducto.getPrecio());
		
		vendedorProductoResource.setProducto(productoService
				.obtenerProductoPorId(vendedorProducto.getProductoIdProducto()));
		
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setIdVendedor(vendedorProducto.getVendedor().getIdVendedor());
		vendedorResource.setApellidos(vendedorProducto.getVendedor().getApellidos());
		vendedorResource.setCargo(vendedorProducto.getVendedor().getCargo());
		vendedorResource.setNombres(vendedorProducto.getVendedor().getNombres());
		
		vendedorProductoResource.setVendedor(vendedorResource);
		return vendedorProductoResource;
	}
	
	@GetMapping("/vendedores")
	public List<VendedorResource> obtenerVendedores(){
		List<VendedorResource> lista = new ArrayList<>();
		vendedorService.obtenerVendedores().forEach(vendedor->{
			VendedorResource vendedorResource = new VendedorResource();
			vendedorResource.setApellidos(vendedor.getApellidos());
			vendedorResource.setCargo(vendedor.getCargo());
			vendedorResource.setIdVendedor(vendedor.getIdVendedor());
			vendedorResource.setNombres(vendedor.getNombres());
			lista.add(vendedorResource);
		});
		return lista;
	}

	
}
