package com.everis.apirest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.VendedorProducto;

@Repository
public interface VendedorProductoRepository extends CrudRepository<VendedorProducto, Long>{

	public Optional<VendedorProducto> findById(Long id);
}
