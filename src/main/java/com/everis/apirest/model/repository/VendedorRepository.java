package com.everis.apirest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.Vendedor;

@Repository
public interface VendedorRepository extends CrudRepository<Vendedor, Long>{
	public Optional<Vendedor> findById(Long id);
}
