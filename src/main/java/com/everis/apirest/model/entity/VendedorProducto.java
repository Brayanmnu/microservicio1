package com.everis.apirest.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class VendedorProducto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idVendedorProducto;
	@Column
	private BigDecimal precio;
	@Column
	private Long productoIdProducto;
	@ManyToOne
	private Vendedor vendedor;
}
